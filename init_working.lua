-- Nastaveni wifi jako station a zadani jmena a hesla
wifi.setmode(wifi.STATION);
wifi.sta.config("Linksys43782","raspberrypi");
-- Vypis IP adresy do terminalu
print(wifi.sta.getip());
-- Prirazeni pinu pro ledku
led1 = 3;
gpio.mode(led1, gpio.OUTPUT);

-- Vytvori server TCP ktery nasloucha na portu 80 (http)
srv=net.createServer(net.TCP);
srv:listen(80,function(conn)
    conn:on("receive", function(client,request)
        -- Pokud prijde pozadavek na port 80 jako odpoved se provede anonymni funkce ktera odesle vyslednou stranku
        local buf = "";
        -- zjisteni metody pozadavku, a globalnich promennych
        local _, _, method, path, vars = string.find(request, "([A-Z]+) (.+)?(.+) HTTP");
        if(method == nil)then
            _, _, method, path = string.find(request, "([A-Z]+) (.+) HTTP");
        end;
        -- Pokud prijde metoda pozadavek metodou GET najde ktere to jsou promene
        local _GET = {}
        if (vars ~= nil)then
            for k, v in string.gmatch(vars, "(%w+)=(%w+)&*") do
                _GET[k] = v;
            end;
        end;
        -- promenna buf se pote odesila zpet klientovi jako HTML
        buf = buf.."<h1> ESP8266 Web Server</h1>";
        buf = buf.."<p>GPIO0 <a href=\"?pin=ON1\"><button>ON</button></a>&nbsp;<a href=\"?pin=OFF1\"><button>OFF</button></a></p>";
   
        -- pravujeme s globalni promenou - podle hodnoty nastavujeme vystup na led diodu
        if (_GET.pin == "ON1") then
              gpio.write(led1, gpio.HIGH);
        elseif (_GET.pin == "OFF1") then
              gpio.write(led1, gpio.LOW);
        end;
        
        -- klientovi se odeslou data
        client:send(buf);
        -- uzavre se komunikace
        client:close();
        -- vycisti se pamet
        collectgarbage();
       
    end)
end)
